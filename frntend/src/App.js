import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";

import CreateCandidate from "./components/create-candidate.component";
import CandidatesList from "./components/candidates-list.component";
import CandidateDetail from './components/candidate-detail.component';

import logo from './logo.png';

class App extends Component {
  render(){
    return (
      <Router>
          <div className="container">
            <nav className="navbar navbar-expand-lg navbar-light bg-primary">
              <a className="navbar-brand" href="https://roshanbhandari08.com.np/" target="_blank">
                <img src={logo} width="30" height="30" />
              </a>
              <Link to="/" className="navbar-brand">Candidates</Link>
                  <ul className="navbar-nav mr-auto">
                    <li className="navbar-item">
                      <Link to="/create" className="nav-link">Create</Link>
                    </li>
                  </ul>
            </nav>
            <Route path="/" exact component={CandidatesList} />
            <Route path="/create" component={CreateCandidate} />
            <Route path="/detail/:id" component={CandidateDetail} />
          </div>
      </Router>
      );
  }
}

export default App;