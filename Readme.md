**Note: This is my first time with nest and was little familiar with react.**

# Installation steps
- Download or clone this project
- open terminal inside project
- cd backend
- npm install
- npm run start:dev
- open another terminal inside root project
- cd frntend
- npm install
- npm start

# Package installed
1. Backend
- [csvtojson](https://www.npmjs.com/package/csvtojson) to read csv file and convert data to json object
- [json2csv](https://www.npmjs.com/package/json2csv) to convert json object to csv format
- [class-validator](https://www.npmjs.com/package/class-validator) to validate model data before insert

2. Frontend
- [bootstrap](https://www.npmjs.com/package/bootstrap) for better ui
- [react-router-dom](https://www.npmjs.com/package/react-router-dom) for routing purpose.
- [axios](https://www.npmjs.com/package/axios) for fetching data from backend.
- [react-bootstrap](https://react-bootstrap.github.io/) for using bootstrap component easily.
- [react-strap](https://reactstrap.github.io/) for pagination.
- [react-datepicker](https://www.npmjs.com/package/react-datepicker) for date input.
- [react-phone-input-2](https://www.npmjs.com/package/react-phone-input-2) for phone number input.
- [react-select-country-list](https://www.npmjs.com/package/react-select-country-list) for nationality input.
- [react-select](https://react-select.com/home) for nationality input.
