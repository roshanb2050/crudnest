import { Controller, Post, Body, Get, Param, Patch, Delete, UsePipes, ValidationPipe } from '@nestjs/common';
import { CandidatesService } from './candidates.service';
import { Candidate } from './candidate.model';
import { validate } from 'class-validator';


@Controller('candidates')
@UsePipes(new ValidationPipe())
export class CandidatesController {
    constructor(private readonly candidateService: CandidatesService) { }
    // saving candidate data
    @Post()
    addCandidate(@Body() newCandidate: Candidate) {
        const generatedId= this.candidateService.insertCandidate(newCandidate);
        return { id:generatedId}
    }

    // getting all candidate data
    @Get()
    getAllCandidates(){
        return this.candidateService.fetchCandidates();
    }

    // getting a single candidate data
    @Get(':id')
    getCandidate(@Param('id') candidateId:number){
        return this.candidateService.fetchSingleCandidate(candidateId);
    }

    // updating a candidate
    // @Patch(':id')
    // updateCandidate(@Param('id') candidateId:number,@Body('title') candidateTitle: string, @Body('desc') candidateDesc:string){
    //     return this.candidateService.updateCandidate(candidateId,candidateTitle,candidateDesc);
    // }

    // deleting a candidate
    // @Delete(':id')
    // removeCandidate(@Param('id') candidateId:number ){
    //     return this.candidateService.deleteCandidate(candidateId);
    // }
}
