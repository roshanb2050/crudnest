import { Injectable, NotFoundException } from '@nestjs/common';
import {Candidate} from './candidate.model'


@Injectable()
export class CandidatesService {
    
    private CsvToJson= require('csvtojson');
    private JsonToCsv=require('json2csv').parse;
    private fs = require('fs');
    private file= "./source.csv";    

    private candidates: Candidate[] =[];
    constructor(){
        // getting csv data and converting to json
        this.CsvToJson()
        .fromFile(this.file)
        .then((jsonObj)=>{
            this.candidates=jsonObj;
        })
    }

    // insert data
    insertCandidate(newCandidate: Candidate) {
        const cId=Math.random();
        // newCandidate.id=cId;
        // convert json object to csv
        var csv ="\n "+cId+"\,"+ this.JsonToCsv(newCandidate,{header:false});
        // append data to file
        this.fs.appendFile(this.file,csv, 'utf8',
        // callback function
        function(err) { 
            if (err) throw err;
            // if no error
        });
        this.candidates.push(newCandidate);
        return cId;
    }

    // fetch all candidates
    fetchCandidates(){
        // getting csv data and converting to json
        return [...this.candidates];
    }

    // fetch single candidate
    fetchSingleCandidate(candidateId: number){
        const candidate=this.findCandidate(candidateId)[0];
        return { ...candidate };
    }

    // update single candidate
    // updateCandidate(candidateId: number, title: string, desc:string){
    //     // const candidate=this.findCandidate(candidateId)[0];
    //     // const index=this.findCandidate(candidateId)[1];
    //     const [candidate, index]=this.findCandidate(candidateId);
    //     const updatedCandidate={...candidate};
    //     if(title){
    //         updatedCandidate.title=title;
    //     }
    //     if(desc){
    //         updatedCandidate.desc=desc;
    //     }
    //     this.candidates[index]=updatedCandidate;
    // }

    // removing a candidate
    // deleteCandidate(candidateId: number){
    //     const index=this.findCandidate(candidateId)[1];
    //     this.candidates.splice(index,1);
    // }

    // return a candidate
    private findCandidate(id: number): any {
        const candidate =this.candidates.filter(function(x){ return x.id == id; });
        const candidateIndex=id;
        if(candidate.length === 0){
            throw new NotFoundException('Could not found candidate.');
        } 
        return [candidate, candidateIndex];
       
    }
}
